package fr.telecomstetienne.Projet_starter_POC_BACK;

import fr.telecomstetienne.Projet_starter_POC_BACK.dao.DailyTimeDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.dao.ProjectDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.dao.UserDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "test")
@AutoConfigureMockMvc
public class DailyTimeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DailyTimeDao dailyTimeDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProjectDao projectDao;

    @Test
    public void getDailyTimeOfUserTest() throws Exception {
        List<User> users = userDao.findAllUsers();
        for (User user : users) {
            int numberOfDTs = 0;
            if (user.getLogin().equals("alexandre.guillermin@telecom-st-etienne.fr")) {
                numberOfDTs = 2;
            } else if (user.getLogin().equals("arnaud.guibert@telecom-st-etienne.fr")) {
                numberOfDTs = 1;
            } else if (user.getLogin().equals("adrien.midol-monnet@telecom-st-etienne.fr")){
                numberOfDTs = 3;
            }
            mockMvc.perform(MockMvcRequestBuilders
                    .get("/dailyTimes?idUser=" + user.getIdUser())
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$", hasSize(numberOfDTs)));
        }
    }

    @Test
    public void postDailyTimeTest() throws Exception {
        Long idUser = userDao.findAllUsers().get(0).getIdUser();
        Long idProject = projectDao.findAllProjects().get(0).getIdProject();
        int numberOfDT = dailyTimeDao.findAllDailyTimeOfUserId(idUser).size();
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                .post("/dailyTimes")
                .content("{\"idUser\":\"" + idUser + "\"," +
                        "\"idProject\":\"" + idProject + "\"," +
                        "\"time\":\"0.125\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        int numberOfDTAfterRequest = dailyTimeDao.findAllDailyTimeOfUserId(idUser).size();
        Long idNewDT = Long.parseLong(response.getResponse().getContentAsString());
        dailyTimeDao.deleteDailyTimeById(idNewDT);
        assertEquals(numberOfDT+1, numberOfDTAfterRequest);
    }
}
