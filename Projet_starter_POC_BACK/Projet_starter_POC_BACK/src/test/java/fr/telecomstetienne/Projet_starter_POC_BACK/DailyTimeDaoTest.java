package fr.telecomstetienne.Projet_starter_POC_BACK;

import fr.telecomstetienne.Projet_starter_POC_BACK.dao.DailyTimeDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.dao.UserDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "test")
public class DailyTimeDaoTest {

    @Autowired
    private UserDao userDao;

    @Autowired
    private DailyTimeDao dailyTimeDao;

    @Test
    public void findAllDailyTimeOfUserId() {
        List<User> users = userDao.findAllUsers();
        for (User user : users) {
            int numberOfDTs = dailyTimeDao.findAllDailyTimeOfUserId(user.getIdUser()).size();
            if (user.getLogin().equals("alexandre.guillermin@telecom-st-etienne.fr")) {
                assertEquals(2, numberOfDTs);
            } else if (user.getLogin().equals("arnaud.guibert@telecom-st-etienne.fr")) {
                assertEquals(1, numberOfDTs);
            } else if (user.getLogin().equals("adrien.midol-monnet@telecom-st-etienne.fr")) {
                assertEquals(3, numberOfDTs);
            }
            else {
                assertEquals(0, numberOfDTs);
            }
        }
    }
}
