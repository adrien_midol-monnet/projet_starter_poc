package fr.telecomstetienne.Projet_starter_POC_BACK;

import fr.telecomstetienne.Projet_starter_POC_BACK.dao.UserDao;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "test")
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void findAllUsersTest() {
        assertEquals(5, userDao.findAllUsers().size());
    }

    @Test
    public void findUsersOfManagerIdTest() {
        Long idManager = userDao.findUserByLogin("arnaud.guibert@telecom-st-etienne.fr").getIdUser();
        assertEquals(2, userDao.findUsersOfManagerId(idManager).size());
    }
}
