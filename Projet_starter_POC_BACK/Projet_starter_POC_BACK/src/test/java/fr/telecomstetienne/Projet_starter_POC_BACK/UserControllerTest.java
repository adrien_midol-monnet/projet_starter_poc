package fr.telecomstetienne.Projet_starter_POC_BACK;

import fr.telecomstetienne.Projet_starter_POC_BACK.dao.RoleDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.dao.UserDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.User;
import fr.telecomstetienne.Projet_starter_POC_BACK.utils.Constants;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "test")
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private UserDao userDao;
    
    @Autowired
    private RoleDao roleDao;

    @Test
    public void postLoginTest() throws Exception {
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                .post("/users")
                .content("{\"login\":\"alexandre.guillermin@telecom-st-etienne.fr\"," +
                        "\"password\":\"Toto1234!\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        String userJsonString = response.getResponse().getContentAsString();
        JSONObject userJson = new JSONObject(userJsonString);
        assertEquals("alexandre.guillermin@telecom-st-etienne.fr", userJson.get("login"));
    }

    @Test
    public void getUsersOfManagerIdTest() throws Exception {
        Long idManager = userDao.findUserByLogin("arnaud.guibert@telecom-st-etienne.fr").getIdUser();
        mockMvc.perform(MockMvcRequestBuilders
                .get("/users?idManager=" + idManager)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }
    
    @Test
    public void postUserTest() throws Exception {
        int numberOfUsers = userDao.findAllUsers().size();
        
        String userLogin = "remi.huguenot@telecom-st-etienne.fr";
        String userPassword = "password";
        Long idManager = userDao.findUserByLogin("arnaud.guibert@telecom-st-etienne.fr").getIdUser();
        
        String json = "{\"login\":\"" + userLogin + "\","
        		+ "\"password\":\"" + userPassword + "\","
        		+ "\"idManager\":\"" + idManager + "\"}";
        
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
        		.post("/new_users")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        
        int numberOfUsersAfterRequest = userDao.findAllUsers().size();
        String idNewUserString = response.getResponse().getContentAsString();
        Long idNewUser = Long.parseLong(idNewUserString);
        
        User newUser = userDao.findUserById(idNewUser);
        userDao.deleteUser(newUser);
        
        assertEquals(numberOfUsers + 1, numberOfUsersAfterRequest);
        assertEquals(numberOfUsers, userDao.findAllUsers().size());
    }
    
    @Test
    public void patchManagerOfUserTest() throws Exception {
    	// login Strings
    	String userLogin = "adrien.midol-monnet@telecom-st-etienne.fr";
        String oldManagerLogin = userDao.findUserByLogin(userLogin).getManager().getLogin();
        String newManagerLogin = "alexandre.guillermin@telecom-st-etienne.fr";
        
        String json = "{\"user\":\"" + userLogin + "\"," + "\"newManager\":\"" + newManagerLogin + "\"}";
        
        mockMvc.perform(MockMvcRequestBuilders
        		.patch("/manager")
        		.content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        
        User user = userDao.findUserByLogin(userLogin);
        User oldManager = userDao.findUserByLogin(oldManagerLogin);
        User newManager = userDao.findUserByLogin(newManagerLogin);
        
        assertEquals(user.getManager().getLogin(), "alexandre.guillermin@telecom-st-etienne.fr");
        assertEquals(oldManager.getManagedUsers().size(), 1);
        assertEquals(newManager.getManagedUsers().size(), 1);
        
        // revert operation back
        json = "{\"user\":\"" + userLogin + "\"," + "\"newManager\":\"" + oldManagerLogin + "\"}";
        
        mockMvc.perform(MockMvcRequestBuilders
        		.patch("/manager")
        		.content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    
    @Test
    public void patchRoleOfUserTest() throws Exception {
    	// login Strings
    	Long idUser = userDao.findUserByLogin("adrien.midol-monnet@telecom-st-etienne.fr").getIdUser();
    	Long idManager = userDao.findUserByLogin("arnaud.guibert@telecom-st-etienne.fr").getIdUser();
    	Long idRole = Constants.ROLE_ADMIN_ID;
        
        String json = "{\"idUser\":\"" + idUser + "\","
        		+ "\"idRole\":\"" + idRole + "\","
        		+ "\"idManager\":\"" + idManager + "\"}";
        
        mockMvc.perform(MockMvcRequestBuilders
        		.patch("/role")
        		.content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        
        User user = userDao.findUserById(idUser);
        idRole = Constants.ROLE_USER_ID;
        
        assertEquals(user.getRole().getIdRole(), Constants.ROLE_ADMIN_ID);
        
        // revert operation back
        json = "{\"idUser\":\"" + idUser + "\","
        		+ "\"idRole\":\"" + idRole + "\","
        		+ "\"idManager\":\"" + idManager + "\"}";
        
        mockMvc.perform(MockMvcRequestBuilders
        		.patch("/role")
        		.content(json)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
    
    /* Static JSon Conversion method */
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
