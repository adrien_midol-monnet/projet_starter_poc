package fr.telecomstetienne.Projet_starter_POC_BACK;

import fr.telecomstetienne.Projet_starter_POC_BACK.dao.ProjectDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Project;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "test")
@AutoConfigureMockMvc
public class ProjectControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProjectDao projectDao;

    @Test
    public void postProjectTest() throws Exception{
        int numberOfProjects = projectDao.findAllProjects().size();
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                .post("/projects")
                .content("Nouveau projet")
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("Nouveau projet")))
                .andReturn();
        int numberOfProjectsAfterRequest = projectDao.findAllProjects().size();
        String projectJsonString = response.getResponse().getContentAsString();
        JSONObject projectJson = new JSONObject(projectJsonString);
        Long idNewProject = projectJson.getLong("idProject");
        assertNotNull(idNewProject);
        Project newProject = projectDao.findProjectById(idNewProject);
        projectDao.deleteProject(newProject);
        assertEquals(numberOfProjects+1, numberOfProjectsAfterRequest);
        assertEquals(numberOfProjects, projectDao.findAllProjects().size());
    }

    @Test
    public void getAllProjectsTest() throws Exception{
        int numberOfProjects = projectDao.findAllProjects().size();
        MvcResult response = mockMvc.perform(MockMvcRequestBuilders
                .get("/projects")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(numberOfProjects)))
                .andReturn();
    }
}
