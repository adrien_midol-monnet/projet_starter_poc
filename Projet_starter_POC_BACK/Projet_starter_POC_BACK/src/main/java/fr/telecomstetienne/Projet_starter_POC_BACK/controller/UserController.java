package fr.telecomstetienne.Projet_starter_POC_BACK.controller;


import fr.telecomstetienne.Projet_starter_POC_BACK.dao.RoleDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.dao.UserDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Role;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.User;
import fr.telecomstetienne.Projet_starter_POC_BACK.utils.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserDao userDao;
    
    @Autowired
    RoleDao roleDao;

    @PostMapping(path="/users", consumes = "application/json", produces = "application/json")
    public User postLogin(@RequestBody Map<String, String> json) {
        String login = json.get("login");
        String password = json.get("password");
        return userDao.login(login, password);
    }

    @GetMapping(path="/users")
    public List<User> getUsersOfManagerId(@RequestParam(name="idManager") Long idManager) {
        return userDao.findUsersOfManagerId(idManager);
    }
    
    @GetMapping(path="/user")
    public User getUserById(@RequestParam(name="idUser") Long idUser) {
        return userDao.findUserById(idUser);
    }    
    
       
    @PostMapping(path="/new_users", consumes = "application/json", produces = "application/json")
    public Long postUser(@RequestBody Map<String, String> json) {
    	String login = json.get("login");
    	String password = json.get("password");
    	Role role = new Role();
        role.setIdRole(Constants.ROLE_USER_ID);
        role.setTitle(Constants.ROLE_USER_TITLE);
    	Long idManager = Long.parseLong(json.get("idManager"));
    	return userDao.createUser(login, password, role, idManager);
    }
    
    @PatchMapping(path="/manager", consumes = "application/json", produces = "application/json")
    public Boolean patchManagerOfUser(@RequestBody Map<String, String> json) {
    	User user = userDao.findUserByLogin(json.get("user"));
    	User newManager = userDao.findUserByLogin(json.get("newManager"));
    	userDao.changeManager(user, newManager);
    	return true;
    }
    
    @PatchMapping(path="/role", consumes = "application/json", produces = "application/json")
    public void patchRoleOfUser(@RequestBody Map<String, String> json) {
    	User user = userDao.findUserById(Long.parseLong(json.get("idUser")));
    	User manager = userDao.findUserById(Long.parseLong(json.get("idManager")));
    	Role role = roleDao.findById(Long.parseLong(json.get("idRole")));
    	userDao.changeRole(user, role, manager);
    }
}
