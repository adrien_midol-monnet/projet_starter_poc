package fr.telecomstetienne.Projet_starter_POC_BACK.dao;

import fr.telecomstetienne.Projet_starter_POC_BACK.domain.DailyTime;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Project;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class DailyTimeDao {
    private static final Logger LOG = LoggerFactory.getLogger(UserDao.class);
    private EntityManager em;
    @Autowired
    private UserDao userDao;
    @Autowired
    private ProjectDao projectDao;

    @PersistenceContext
    public void setEm(EntityManager em) {
        this.em = em;
    }

    public DailyTime createDailyTime(LocalDate localDate, User user, Project project, Double time) {
        DailyTime dt = new DailyTime();
        dt.setDate(localDate);
        dt.setUserAndProject(user, project);
        dt.setTime(time);
        em.persist(dt);
        LOG.info(dt + " saved to database.");
        return dt;
    }

    public void updateDailyTime(DailyTime dt) {
        em.merge(dt);
        LOG.info(dt + " updated in database.");
    }

    public void deleteDailyTime(DailyTime dt) {
        DailyTime fromDB = em.getReference(DailyTime.class, dt.getIdDailyTime());
        User user = fromDB.getUser();
        user.getDailyTimes().remove(fromDB);
        userDao.updateUser(user);
        Project project = fromDB.getProject();
        project.getDailyTimes().remove(fromDB);
        projectDao.updateProject(project);
        em.remove(fromDB);
        LOG.info(dt + " removed from database.");
    }

    public void deleteDailyTimeById(Long id) {
        DailyTime fromDB = em.getReference(DailyTime.class, id);
        User user = fromDB.getUser();
        user.getDailyTimes().remove(fromDB);
        userDao.updateUser(user);
        Project project = fromDB.getProject();
        project.getDailyTimes().remove(fromDB);
        projectDao.updateProject(project);
        em.remove(fromDB);
        LOG.info(fromDB + " removed from database.");
    }

    public List<DailyTime> findAllDailyTimeOfUserId(Long idUser) {
        Query q = em.createQuery(
                "SELECT DISTINCT dt " +
                        "FROM DailyTime dt JOIN dt.user u " +
                        "WHERE u.id = :id");
        q.setParameter("id", idUser);
        return q.getResultList();
    }
}
