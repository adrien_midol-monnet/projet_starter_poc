package fr.telecomstetienne.Projet_starter_POC_BACK.dao;

import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class RoleDao {
    private static final Logger LOG = LoggerFactory.getLogger(UserDao.class);
    private EntityManager em;

    @PersistenceContext
    public void setEm(EntityManager em) {
        this.em = em;
    }

    public void createRole(Long id, String title) {
        Role role = new Role();
        role.setIdRole(id);
        role.setTitle(title);
        em.persist(role);
        LOG.info(role + " saved to database.");
    }

    public void updateRole(Role role) {
        em.merge(role);
        LOG.info(role + " updated in database.");
    }

    public void deleteRole(Role role) {
        Role fromDB = em.getReference(Role.class, role.getIdRole());
        em.remove(fromDB);
        LOG.info(role + " removed from database.");
    }

    public Role findById(Long id) {
        return em.find(Role.class, id);
    }
}
