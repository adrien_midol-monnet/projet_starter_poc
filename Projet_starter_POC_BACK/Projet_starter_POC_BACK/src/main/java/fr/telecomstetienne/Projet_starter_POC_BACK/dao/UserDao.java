package fr.telecomstetienne.Projet_starter_POC_BACK.dao;

import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Role;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.User;
import fr.telecomstetienne.Projet_starter_POC_BACK.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class UserDao {
    private static final Logger LOG = LoggerFactory.getLogger(UserDao.class);
    private EntityManager em;

    @PersistenceContext
    public void setEm(EntityManager em) {
        this.em = em;
    }

    public Long createUser(String login, String password, Role role, Long idManager) {
        User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setRole(role);
        
        User manager = null;
        if (idManager != null) {
            manager = this.findUserById(idManager);
        }
        user.setManager(manager);
        if (!role.getTitle().equals(Constants.ROLE_MANAGER_TITLE)) {
            user.setManagedUsers(null);
        }
        else {
            user.setManagedUsers(new ArrayList<User>());
        }
        em.persist(user);
        if (role.getTitle().equals(Constants.ROLE_USER_TITLE)) {
            manager.addManagedUser(user);
            this.updateUser(manager);
        }
        LOG.info(user + " saved to database.");
        return user.getIdUser();
    }

    public User findUserById(Long id) {
        return em.find(User.class, id);
    }

    public User findUserByLogin(String login) {
        Query q = em.createQuery("SELECT DISTINCT u FROM User u WHERE u.login = :login");
        q.setParameter("login", login);
        
        List<User> result = q.getResultList();
        if (result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }
    
    public void updateUser(User user) {
        em.merge(user);
        LOG.info(user + " updated in database.");
	}
    
    public void deleteUser(User user) {
		User fromDB = em.getReference(User.class, user.getIdUser());
		if (fromDB.getRole().getTitle().equals(Constants.ROLE_USER_TITLE)) {
			User manager = fromDB.getManager();
			manager.removeManagedUser(fromDB);
			
			fromDB.setManager(null);
			updateUser(manager);
		}
		em.remove(fromDB);
		LOG.info(user + " deleted from database.");
	}
    
    public void changeManager(User user, User newManager) {
		User fromDB = em.getReference(User.class, user.getIdUser());
		if (fromDB.getRole().getTitle().equals(Constants.ROLE_USER_TITLE)) {
			User oldManager = fromDB.getManager();
			oldManager.removeManagedUser(fromDB);
			updateUser(oldManager);
			
			fromDB.setManager(newManager);
			newManager.addManagedUser(user);
			updateUser(newManager);
		}
		updateUser(fromDB);
	}
    
    public void changeRole(User user, Role role, User manager) {
		User fromDB = em.getReference(User.class, user.getIdUser());
		fromDB.setRole(role);
		
		if (role.getTitle().equals(Constants.ROLE_USER_TITLE)) {
			changeManager(user, manager);
		}
		else {
			updateUser(user);
		}
	}
    
    public List<User> findAllUsers() {
        Query q = em.createQuery("FROM User");
        List<User> list = q.getResultList();
        return list;
    }
    
    public List<User> findUsersOfManagerId(Long idManger) {
        Query q = em.createQuery("SELECT DISTINCT u FROM User u JOIN u.manager m WHERE m.id = :id");
        q.setParameter("id", idManger);
        return q.getResultList();
    }

    public User login(String login, String password) {
        User user = this.findUserByLogin(login);
        if (user != null) {
        	if (user.verifyPassword(password)) {
        		System.out.println(user.getLogin());
        		return user;
        	}
        }
        return null;        
    }
}
