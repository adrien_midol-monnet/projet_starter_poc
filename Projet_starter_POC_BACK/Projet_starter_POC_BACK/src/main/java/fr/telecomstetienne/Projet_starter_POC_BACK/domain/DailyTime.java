package fr.telecomstetienne.Projet_starter_POC_BACK.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class DailyTime {
    private @Id @GeneratedValue Long idDailyTime;
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate date;
    @ManyToOne
    @JsonIgnoreProperties("dailyTimes")
    @JoinColumn(name="USER_ID", nullable=false)
    private User user;
    @ManyToOne
    @JsonIgnoreProperties("dailyTimes")
    @JoinColumn(name="PROJECT_ID", nullable=false)
    private Project project;
    private Double time;

    public void setUserAndProject(User user_, Project project_) {
        this.user = user_;
        user_.addDailyTime(this);
        this.project = project_;
        project_.addDailyTime(this);
    }
}
