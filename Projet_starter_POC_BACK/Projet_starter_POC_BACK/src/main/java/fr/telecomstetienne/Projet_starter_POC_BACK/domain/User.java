package fr.telecomstetienne.Projet_starter_POC_BACK.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@Entity
@JsonIgnoreProperties({"password"})
public class User {
    private @Id @GeneratedValue Long idUser;
    private String login;
    private String password;
    
    @ManyToOne(fetch = FetchType.EAGER, cascade={CascadeType.ALL})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnoreProperties({"user", "dailyTimes", "managedUsers"})
    @JoinColumn(name="MANAGER_ID")
    private User manager;
    
    @OneToMany(mappedBy="manager", fetch = FetchType.EAGER, cascade={CascadeType.ALL})
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnoreProperties("manager")
    private List<User> managedUsers;
    
    @ManyToOne
    private Role role;
    
    @OneToMany(mappedBy="user", fetch = FetchType.EAGER, cascade={CascadeType.ALL}, orphanRemoval=true)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnoreProperties("user")
    private Set<DailyTime> dailyTimes;

    public void addDailyTime(DailyTime dt) {
        this.dailyTimes.add(dt);
    }

    public void addManagedUser(User user) {
        this.managedUsers.add(user);
    }
    
    public void removeManagedUser(User user) {
    	this.managedUsers.remove(user);
    }
    
    public boolean verifyPassword(String password_) { return this.password.equals(password_); }
}
