package fr.telecomstetienne.Projet_starter_POC_BACK.utils;

public class Constants {
    final public static Long ROLE_USER_ID = 1L;
    final public static Long ROLE_MANAGER_ID = 2L;
    final public static Long ROLE_ADMIN_ID = 3L;

    final public static String ROLE_USER_TITLE = "USER";
    final public static String ROLE_MANAGER_TITLE = "MANAGER";
    final public static String ROLE_ADMIN_TITLE = "ADMIN";
}
