package fr.telecomstetienne.Projet_starter_POC_BACK.dao;

import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class ProjectDao {
    private static final Logger LOG = LoggerFactory.getLogger(UserDao.class);
    private EntityManager em;

    @PersistenceContext
    public void setEm(EntityManager em) {
        this.em = em;
    }

    public Project createProject(String name) {
        Project project = new Project();
        project.setName(name);
        em.persist(project);
        LOG.info(project + " saved to database.");
        return project;
    }

    public Project findProjectById(Long id) {
        return em.getReference(Project.class, id);
    }

    public void updateProject(Project project) {
        em.merge(project);
        LOG.info(project + " updated in database.");
    }

    public void deleteProject(Project project) {
        Project fromDB = em.getReference(Project.class, project.getIdProject());
        em.remove(fromDB);
        LOG.info(fromDB + " removed from database.");
    }

    public List<Project> findAllProjects() {
        Query q = em.createQuery("FROM Project");
        List<Project> list = q.getResultList();
        return list;
    }
}
