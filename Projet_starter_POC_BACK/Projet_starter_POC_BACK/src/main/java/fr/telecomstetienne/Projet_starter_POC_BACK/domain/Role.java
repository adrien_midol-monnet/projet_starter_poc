package fr.telecomstetienne.Projet_starter_POC_BACK.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Role {
    private @Id Long idRole;
    private String title;
}
