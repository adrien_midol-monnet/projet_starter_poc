package fr.telecomstetienne.Projet_starter_POC_BACK.controller;

import fr.telecomstetienne.Projet_starter_POC_BACK.dao.DailyTimeDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.dao.ProjectDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.dao.UserDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.DailyTime;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Project;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;

@RestController
@CrossOrigin
public class DailyTimeController {

    @Autowired
    DailyTimeDao dailyTimeDao;

    @Autowired
    UserDao userDao;

    @Autowired
    ProjectDao projectDao;

    @GetMapping(path = "/dailyTimes")
    public Collection<DailyTime> getDailyTimeOfUser(@RequestParam(name = "idUser") Long idUser) {
        return dailyTimeDao.findAllDailyTimeOfUserId(idUser);
    }

    @PostMapping(path="/dailyTimes", consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Long postNewDailyTime(@RequestBody Map<String, String> json) {
        Long idUser = Long.parseLong(json.get("idUser"));
        Long idProject = Long.parseLong(json.get("idProject"));
        Double time = Double.parseDouble(json.get("time"));
        User user = userDao.findUserById(idUser);
        Project project = projectDao.findProjectById(idProject);
        Long idDT = dailyTimeDao.createDailyTime(LocalDate.now(), user, project, time).getIdDailyTime();
        return idDT;
    }
}
