package fr.telecomstetienne.Projet_starter_POC_BACK.utils;


import fr.telecomstetienne.Projet_starter_POC_BACK.dao.*;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.DailyTime;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Project;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Role;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.List;

@Configuration
@Slf4j
public class LoadDatabase {

    @Bean
    @Profile("!test")
    CommandLineRunner initDatabase(RoleDao roleDao, UserDao userDao, ProjectDao projectDao, DailyTimeDao dailyTimeDao) {

        return args -> {
            initRoles(roleDao);
            initUsers(userDao,roleDao);
            initProjects(projectDao);
            initDailyTime(dailyTimeDao, userDao, projectDao);
        };
    }

    private void initUsers(UserDao userDao, RoleDao roleDao) {
        Role userRole = roleDao.findById(1L);
        Role managerRole = roleDao.findById(2L);
        Role adminRole = roleDao.findById(3L);

        userDao.createUser("alexandre.guillermin@telecom-st-etienne.fr",
                "Toto1234!", adminRole, null);
        userDao.createUser("manager.dupont@telecom-st-etienne.fr",
                "M4n4g3r", managerRole, null);
        Long idManager = userDao.createUser("arnaud.guibert@telecom-st-etienne.fr",
                "abc123", managerRole, null);
        userDao.createUser("adrien.midol-monnet@telecom-st-etienne.fr",
                "azertyuiop1234567890", userRole, idManager);
        userDao.createUser("leon.doudhou@telecom-st-etienne.fr",
                "Toto1234!", userRole, idManager);
    }

    private void initProjects(ProjectDao projectDao) {
        projectDao.createProject("Archi n-tiers");
        projectDao.createProject("Startup_POC");
        projectDao.createProject("METADEV");
    }

    private void initDailyTime(DailyTimeDao dailyTimeDao, UserDao userDao,
                               ProjectDao projectDao) {
        User user0 = userDao.findUserByLogin("alexandre.guillermin@telecom-st-etienne.fr");
        User user1 = userDao.findUserByLogin("arnaud.guibert@telecom-st-etienne.fr");
        User user2 = userDao.findUserByLogin("adrien.midol-monnet@telecom-st-etienne.fr");
        List<Project> projects = projectDao.findAllProjects();
        dailyTimeDao.createDailyTime(LocalDate.now(), user0, projects.get(1), 0.5);
        dailyTimeDao.createDailyTime(LocalDate.now(), user0, projects.get(2), 0.5);

        dailyTimeDao.createDailyTime(LocalDate.now(), user1, projects.get(0), 1.0);

        dailyTimeDao.createDailyTime(LocalDate.now(), user2, projects.get(2), 0.25);
        dailyTimeDao.createDailyTime(LocalDate.now(), user2, projects.get(0), 0.25);
        dailyTimeDao.createDailyTime(LocalDate.now(), user2, projects.get(1), 0.5);
    }

    private void initRoles(RoleDao roleDao) {
        roleDao.createRole(Constants.ROLE_USER_ID, Constants.ROLE_USER_TITLE);
        roleDao.createRole(Constants.ROLE_MANAGER_ID, Constants.ROLE_MANAGER_TITLE);
        roleDao.createRole(Constants.ROLE_ADMIN_ID, Constants.ROLE_ADMIN_TITLE);
    }

    @Bean
    @Profile("test")
    CommandLineRunner initTestDatabase(RoleDao roleDao,
                                       ProjectDao projectDao,
                                       DailyTimeDao dailyTimeDao,
                                       UserDao userDao) {

        return args -> {
            initRoles(roleDao);
            initUsers(userDao, roleDao);
            initProjects(projectDao);
            initDailyTime(dailyTimeDao, userDao, projectDao);
        };
    }
}

