package fr.telecomstetienne.Projet_starter_POC_BACK.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
public class Project {
    private @Id @GeneratedValue Long idProject;
    private String name;
    @OneToMany(mappedBy="project", fetch = FetchType.EAGER, cascade={CascadeType.ALL}, orphanRemoval=true)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnoreProperties("project")
    private Set<DailyTime> dailyTimes;

    public void addDailyTime(DailyTime dt) {
        dailyTimes.add(dt);
    }
}
