package fr.telecomstetienne.Projet_starter_POC_BACK;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetStarterPocBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetStarterPocBackApplication.class, args);
	}

}
