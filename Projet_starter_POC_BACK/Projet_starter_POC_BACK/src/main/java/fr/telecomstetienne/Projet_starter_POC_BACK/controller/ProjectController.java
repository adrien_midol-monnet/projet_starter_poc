package fr.telecomstetienne.Projet_starter_POC_BACK.controller;

import fr.telecomstetienne.Projet_starter_POC_BACK.dao.ProjectDao;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.Project;
import fr.telecomstetienne.Projet_starter_POC_BACK.domain.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class ProjectController {
    @Autowired
    ProjectDao projectDao;

    @PostMapping(path = "/projects")
    @ResponseStatus(HttpStatus.CREATED)
    public Project postProject(@RequestBody String name) {
        return projectDao.createProject(name);
    }
    
    @GetMapping(path="/projects")
    public List<Project> getAllProjects() {
        return projectDao.findAllProjects();
    }   
    
}
